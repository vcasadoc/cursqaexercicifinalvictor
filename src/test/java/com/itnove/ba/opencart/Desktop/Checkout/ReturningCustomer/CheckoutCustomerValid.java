package com.itnove.ba.opencart.Desktop.Checkout.ReturningCustomer;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.CheckoutPage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class CheckoutCustomerValid extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {


        //Accedir a la url http://opencart.votarem.lu/
        driver.get("http://opencart.votarem.lu");

        //Situar el mouse sobre el botó Add to Cart del producte Macbook de la portada de la botiga.
        CheckoutPage checkout = new CheckoutPage(driver);
        Actions hover = new Actions(driver);
        hover.moveToElement(checkout.addToCartMac).build().perform();

        //Clicar el botó Add to Cart
        checkout.addToCartMac.click();

        //Situar el mouse sobre la opció Checkout de la part superior dreta de la pantalla
        hover.moveToElement(checkout.checkoutButton).build().perform();

        //Fer clic a checkout
        checkout.checkoutButton.click();

        //Clicar al camp de text Email, sota la seccio Returning Customer
        wait.until(ExpectedConditions.elementToBeClickable(checkout.customerEmail));
        checkout.customerEmail.click();

        //Escriure compte valid
        checkout.customerEmail.sendKeys(checkout.valid_email);

        //Clicar al camp de text Password
        checkout.customerPassword.click();

        //Escriure password valid
        checkout.customerPassword.sendKeys(checkout.valid_password);

        //Situar el mouse a sobre del botó LoginMobile
        hover.moveToElement(checkout.customerLoginButton).build().perform();

        //Clicar LoginMobile
        checkout.customerLoginButton.click();

        //Verificar que surt la seccio de billing amb el selector d'adreça
        wait.until(ExpectedConditions.visibilityOf(checkout.addressDisplay));
        assertTrue(checkout.addressDisplay.isDisplayed());



    }

}
