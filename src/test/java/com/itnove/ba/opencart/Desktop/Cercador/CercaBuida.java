package com.itnove.ba.opencart.Desktop.Cercador;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.CercadorPage;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class CercaBuida extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        //Accedir a la url http://opencart.votarem.lu/
        driver.get("http://opencart.votarem.lu");

        //Posar el mouse sobre la lupa del cercador
        CercadorPage cerca = new CercadorPage(driver);
        Actions hover = new Actions(driver);
        hover.moveToElement(cerca.lupa).build().perform();

        //Clicar Lupa
        cerca.lupa.click();

        //Verifiquem que no apareix cap resultat
        assertTrue(cerca.missatgeCarroBuit.getText().equals("Your shopping cart is empty!"));

    }

}
