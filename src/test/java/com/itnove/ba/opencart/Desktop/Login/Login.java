package com.itnove.ba.opencart.Desktop.Login;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.LoginPage;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class Login extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        //Accedir a la url http://opencart.votarem.lu/
        driver.get("http://opencart.votarem.lu");


        //Situar el mouse sobre el botó My Account
        LoginPage login = new LoginPage(driver);
        login.Login(login.valid_email, login.valid_password, driver);

        //Verificar que s'ha fet el login correctament
        wait.until(ExpectedConditions.visibilityOf(login.myAccountLabel));
        assertTrue(login.myAccountLabel.getText().equals("My Account"));



    }

}
