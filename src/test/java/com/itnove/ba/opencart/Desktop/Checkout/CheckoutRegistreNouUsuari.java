package com.itnove.ba.opencart.Desktop.Checkout;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.CheckoutPage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class CheckoutRegistreNouUsuari extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        //Accedir a la url http://opencart.votarem.lu/
        driver.get("http://opencart.votarem.lu");

        //Situar el mouse sobre el botó Add to Cart del producte Macbook de la portada de la botiga.
        CheckoutPage checkout = new CheckoutPage(driver);
        Actions hover = new Actions(driver);
        hover.moveToElement(checkout.addToCartMac).build().perform();

        //Clicar el botó Add to Cart
        checkout.addToCartMac.click();

        //Situar el mouse sobre la opció Checkout de la part superior dreta de la pantalla
        hover.moveToElement(checkout.checkoutButton).build().perform();

        //Fer clic a checkout
        checkout.checkoutButton.click();

        //Clicar a la opció Guest checkout
        wait.until(ExpectedConditions.elementToBeClickable(checkout.radioRegisterAccount));
        checkout.radioRegisterAccount.click();

        //Situar el mouse al botó continue
        hover.moveToElement(checkout.continueGuest).build().perform();

        //Fer clic al botó Continue
        checkout.continueGuest.click();

        //Clicar al camp de text First Name
        wait.until(ExpectedConditions.elementToBeClickable(checkout.textName));
        checkout.textName.click();

        //Escriure John
        checkout.textName.sendKeys("John");

        //Clicar al camp de text Last Name
        checkout.textLastName.click();

        //Escriure Doe
        checkout.textLastName.sendKeys("Doe");

        //Clicar al camp de text email
        checkout.textEmail.click();

        //Escriure prova@prova.com
        checkout.textEmail.sendKeys("prova@prova.com");

        //Clicar al camp de text Telephone
        checkout.textPhone.click();

        //Escriure telefon
        checkout.textPhone.sendKeys("777777777");

        //Escriure pass
        checkout.textPassword.sendKeys("password");

        //Escriure confirmar pass
        checkout.textConfirmPassword.sendKeys("password");

        //Clicar al camp de text Address1
        checkout.textAddress1.click();

        //Escriure Test address 1
        checkout.textAddress1.sendKeys("Test address 1");

        //Clicar al camp de text City
        checkout.textCity.click();

        //Escriure Barcelona
        checkout.textCity.sendKeys("Barcelona");

        //Clicar al camp de text Post Code
        checkout.textPostCode.click();

        //Escriure 08080
        checkout.textPostCode.sendKeys("08080");

        //Fer clic al desplegable Country
        checkout.countryDisplay.click();

        //CLicar Spain
        checkout.countrySpain.click();

        //Fer clic al desplegable Region / State
        checkout.regionDisplay.click();

        //CLicar Barcelona
        //wait.until(ExpectedConditions.elementToBeClickable(checkout.regionBarcelona));
        Thread.sleep(3000);
        checkout.regionBarcelona.click();

        //Check Agreement checkbox
        checkout.checkAgreement.click();

        //Situar Mouse sobre botó Continue
        Thread.sleep(2000);
        hover.moveToElement(checkout.buttonRegister).build().perform();

        //Clicar Continue
        checkout.buttonRegister.click();

        //Clicar el checkbox de la seccio I Have read...
        wait.until(ExpectedConditions.elementToBeClickable(checkout.checkTerms));
        checkout.checkTerms.click();

        //Situar mouse sobre Continue
        hover.moveToElement(checkout.continueToConfirmOrder).build().perform();

        //Clicar continue
        checkout.continueToConfirmOrder.click();

        //Verificar que surt error de metode de pagament
        wait.until(ExpectedConditions.elementToBeClickable(checkout.errorPaymentMethod));
        assertTrue(checkout.errorPaymentMethod.isDisplayed());



    }

}
