package com.itnove.ba.opencart.Desktop.Admin.AdminLogin;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.LoginAdminPage;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class LoginAdminUsuariExistent extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        //Accedir a la url http://opencart.votarem.lu/admin
        driver.get("http://opencart.votarem.lu/admin");


        //Fem el login
        LoginAdminPage login = new LoginAdminPage(driver);
        login.Login(login.valid_user, login.valid_password, driver);

        //Verificar que s'ha fet el login correctament
        wait.until(ExpectedConditions.visibilityOf(login.securityNotification));
        assertTrue(login.securityNotification.isDisplayed());



    }

}
