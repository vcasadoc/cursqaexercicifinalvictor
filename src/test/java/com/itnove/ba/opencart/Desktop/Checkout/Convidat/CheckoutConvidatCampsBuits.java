package com.itnove.ba.opencart.Desktop.Checkout.Convidat;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.Pages.CheckoutPage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

public class CheckoutConvidatCampsBuits extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {

        //Accedir a la url http://opencart.votarem.lu/
        driver.get("http://opencart.votarem.lu");

        //Situar el mouse sobre el botó Add to Cart del producte Macbook de la portada de la botiga.
        CheckoutPage checkout = new CheckoutPage(driver);
        Actions hover = new Actions(driver);
        hover.moveToElement(checkout.addToCartMac).build().perform();

        //Clicar el botó Add to Cart
        checkout.addToCartMac.click();

        //Situar el mouse sobre la opció Checkout de la part superior dreta de la pantalla
        hover.moveToElement(checkout.checkoutButton).build().perform();

        //Fer clic a checkout
        checkout.checkoutButton.click();

        //Clicar a la opció Guest checkout
        wait.until(ExpectedConditions.elementToBeClickable(checkout.radioGuest));
        checkout.radioGuest.click();

        //Situar el mouse al botó continue
        hover.moveToElement(checkout.continueGuest).build().perform();

        //Fer clic al botó Continue
        checkout.continueGuest.click();

        //Situar Mouse sobre botó Continue
        wait.until(ExpectedConditions.elementToBeClickable(checkout.continueToPaymentMethod));
        hover.moveToElement(checkout.continueToPaymentMethod).build().perform();

        //Clicar Continue
        checkout.continueToPaymentMethod.click();

        //Verificar que surt error de camps buits
        wait.until(ExpectedConditions.visibilityOf(checkout.errorRegion));
        //Thread.sleep(3000);
        assertTrue(checkout.errorFirstName.getText().equals("First Name must be between 1 and 32 characters!"));
        assertTrue(checkout.errorLastName.getText().equals("Last Name must be between 1 and 32 characters!"));
        assertTrue(checkout.errorEmail.getText().equals("E-Mail address does not appear to be valid!"));
        assertTrue(checkout.errorPhone.getText().equals("Telephone must be between 3 and 32 characters!"));
        assertTrue(checkout.errorAddress1.getText().equals("Address 1 must be between 3 and 128 characters!"));
        assertTrue(checkout.errorCity.getText().equals("City must be between 2 and 128 characters!"));
        assertTrue(checkout.errorPostCode.getText().equals("Postcode must be between 2 and 10 characters!"));
        assertTrue(checkout.errorRegion.getText().equals("Please select a region / state!"));



    }

}
