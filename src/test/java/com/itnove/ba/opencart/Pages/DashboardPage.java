package com.itnove.ba.opencart.Pages;

import com.itnove.ba.BaseTest;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.testng.AssertJUnit.assertTrue;

public class DashboardPage extends BaseTest {


        private WebDriver driver;


        @FindBy(xpath="//*[@id='modal-security']/div/div/div[1]/button")
        public WebElement closeNotification;

        @FindBy(xpath="//*[@id='menu-catalog']/a")
        public WebElement menuCatalog;

        @FindBy(xpath = "//*[@id='collapse1']/li[2]/a")
        public WebElement menuProducts;

        @FindBy(xpath = "//*[@id='content']/div[1]/div/div/a")
        public WebElement buttonAdd;

        @FindBy(xpath = "//*[@id='input-name1']")
        public WebElement textProdName;

        @FindBy(xpath = "//*[@id='input-meta-title1']")
        public WebElement textMetaTag;

        @FindBy(xpath = "//*[@id='form-product']/ul/li[2]/a")
        public WebElement dataTab;

        @FindBy(xpath = "//*[@id='input-model']")
        public WebElement textModel;

        @FindBy(xpath = "//*[@id='content']/div[1]/div/div/button")
        public WebElement buttonSave;

        @FindBy(xpath = "//*[@id='content']/div[2]/div[1]")
        public WebElement successMessage;

        @FindBy(xpath = "//*[@id='input-name']")
        public WebElement filterProdName;

        @FindBy(xpath = "//*[@id='button-filter']")
        public WebElement filterButton;

        @FindBy(xpath = "//*[@id='form-product']/div/table/tbody/tr[1]/td[1]/input")
        public WebElement checkButton;

        @FindBy(xpath = "//*[@id='content']/div[1]/div/div/button[3]")
        public WebElement deleteButton;

        @FindBy(xpath = "//*[@id='content']/div[2]/div[1]")
        public WebElement successDelete;







        public DashboardPage(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }

        public void AccessProduct(WebDriver driver) {
                //LoginPage login = new LoginPage(driver);
                Actions hover = new Actions(driver);

                //Situar Mouse a catalog
                hover.moveToElement(menuCatalog).build().perform();

                //Clicar catalog
                menuCatalog.click();

                //Situar Mouse a Products
                hover.moveToElement(menuProducts).build().perform();

                //Clicar Products
                menuProducts.click();

        }

        public void CreateProduct(String productName, String metaTag, String model, WebDriver driver) {

                //Situar el mouse sobre el botó amb un simbol de suma, a la part superior dreta
                Actions hover = new Actions(driver);
                hover.moveToElement(buttonAdd).build().perform();

                //CLicar boto add
                buttonAdd.click();

                //Clicar el camp de text Product Name
                textProdName.click();

                //Escriure testproduct
                textProdName.sendKeys(productName);

                //Clicar el camp de text Meta Tag Title
                textMetaTag.click();

                //Escriure testproduct
                textMetaTag.sendKeys(metaTag);

                //A la part superior clicar la pestanya Data
                dataTab.click();

                //Clicar el camp de text Model
                textModel.click();

                //Escriure TestModel
                textModel.sendKeys(model);

                //Situar el mouse sobre el botó de salvar a la part superior dreta
                hover.moveToElement(buttonSave).build().perform();

                //Clicar botó Salvar
                buttonSave.click();

        }

        public void DeleteProduct(String prodName, WebDriver driver) {
                Actions hover = new Actions(driver);

                //Clicar el camp de text de filter, a nom de producte
                filterProdName.click();

                //Escriure nom del producte
                filterProdName.sendKeys(prodName);

                //Mouse over boto filter
                hover.moveToElement(filterButton).build().perform();

                //Clicar a Filter
                filterButton.click();

                //Clicar el checkbutton
                checkButton.click();

                //Mouse over botó Delete
                hover.moveToElement(deleteButton).build().perform();

                //Clic a delete
                deleteButton.click();

                WebDriverWait wait = new WebDriverWait(driver, 10);
                Alert alert = wait.until(ExpectedConditions.alertIsPresent());

                //Accepting alert.
                alert.accept();



        }
}

