package com.itnove.ba.opencart.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutPageMobile {


        private WebDriver driver;

        public String valid_email = "vic.casado@gmail.com";
        public String invalid_email = "test@test.com";
        public String valid_password = "validpassword";
        public String invalid_password = "invalidpassword";

        @FindBy(xpath="//*[@id='content']/div[3]/div[2]/a")
        public WebElement checkoutButton2;

        @FindBy(xpath="//*[@id='top-links']/ul/li[4]")
        public WebElement checkoutButton;

        @FindBy(xpath = "//*[@id='content']/p")
        public WebElement missatgeCarroBuit;

        @FindBy(xpath="//*[@id='content']/div[2]/div[1]/div/div[3]/button[1]")
        public WebElement addToCartMac;

        @FindBy(xpath="//*[@id='common-home']/div[1]")
        public WebElement successMessage;
        //Success: You have added MacBook to your shopping cart!

        @FindBy(xpath="//*[@id='collapse-checkout-option']/div/div/div[1]/div[2]/label/input")
        public WebElement radioGuest;

        @FindBy(xpath="//*[@id='input-email']")
        public WebElement customerEmail;

        @FindBy(xpath="//*[@id='input-password']")
        public WebElement customerPassword;

        @FindBy(xpath="//*[@id='button-login']")
        public WebElement customerLoginButton;

        @FindBy(xpath="//*[@id='collapse-checkout-option']/div/div[1]")
        public WebElement errorCustomerLogin;
        // Warning: No match for E-Mail Address and/or Password.

        @FindBy(xpath="//*[@id='button-account']")
        public WebElement continueGuest;

        @FindBy(xpath="//*[@id='input-payment-firstname']")
        public WebElement textName;

        @FindBy(xpath="//*[@id='input-payment-lastname']")
        public WebElement textLastName;

        @FindBy(xpath="//*[@id='input-payment-email']")
        public WebElement textEmail;

        @FindBy(xpath="//*[@id='input-payment-telephone']")
        public WebElement textPhone;

        @FindBy(xpath="//*[@id='input-payment-address-1']")
        public WebElement textAddress1;

        @FindBy(xpath="//*[@id='input-payment-city']")
        public WebElement textCity;

        @FindBy(xpath="//*[@id='input-payment-postcode']")
        public WebElement textPostCode;

        @FindBy(xpath="//*[@id='input-payment-country']")
        public WebElement countryDisplay;

        @FindBy(xpath="//*[@id='input-payment-country']/option[209]")
        public WebElement countrySpain;

        @FindBy(xpath="//*[@id='input-payment-zone']")
        public WebElement regionDisplay;

        @FindBy(xpath="//*[@id='input-payment-zone']/option[10]")
        public WebElement regionBarcelona;

        @FindBy(xpath="//*[@id='button-guest']")
        public WebElement continueToPaymentMethod;

        @FindBy(xpath="//*[@id='collapse-payment-method']/div/div[2]/div/input[1]")
        public WebElement checkTerms;

        @FindBy(xpath="//*[@id='button-payment-method']")
        public WebElement continueToConfirmOrder;

        @FindBy(xpath="//*[@id='collapse-payment-method']/div/div[1]")
        public WebElement errorPaymentMethod;
        //Warning: Payment method required!

        @FindBy(xpath="//*[@id='account']/div[2]/div")
        public WebElement errorFirstName;

        @FindBy(xpath="//*[@id='account']/div[3]/div")
        public WebElement errorLastName;

        @FindBy(xpath="//*[@id='account']/div[4]/div")
        public WebElement errorEmail;

        @FindBy(xpath="//*[@id='account']/div[5]/div")
        public WebElement errorPhone;

        @FindBy(xpath="//*[@id='address']/div[2]/div")
        public WebElement errorAddress1;

        @FindBy(xpath="//*[@id='address']/div[4]/div")
        public WebElement errorCity;

        @FindBy(xpath="//*[@id='address']/div[5]/div")
        public WebElement errorPostCode;

        @FindBy(xpath="//*[@id='address']/div[7]/div")
        public WebElement errorRegion;

        @FindBy(xpath="//*[@id='payment-existing']/select")
        public WebElement addressDisplay;




        public CheckoutPageMobile(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }


}

