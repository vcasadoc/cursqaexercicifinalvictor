package com.itnove.ba.opencart.Pages;

import com.itnove.ba.BaseTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPageMobile extends BaseTest  {


        private WebDriver driver;

        public String valid_email = "vic.casado@gmail.com";
        public String invalid_email = "test@test.com";
        public String valid_password = "validpassword";
        public String invalid_password = "invalidpassword";

        @FindBy(xpath="//*[@id='top-links']/ul/li[2]/a")
        public WebElement myAccount;

        @FindBy(xpath="//*[@id='top-links']/ul/li[2]/ul/li[2]/a")
        public WebElement loginMenu;

        @FindBy(xpath = "//*[@id='input-email']")
        public WebElement loginEmail;

        @FindBy(xpath = "//*[@id='input-password']")
        public WebElement loginPassword;

        @FindBy(xpath = "//*[@id='content']/div/div[2]/div/form/input")
        public WebElement loginButton;

        @FindBy(xpath = "//*[@id='content']/h2[1]")
        public WebElement myAccountLabel;

        public LoginPageMobile(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }

        public void Login(String compte, String password, WebDriver driver) {

                //LoginPage login = new LoginPage(driver);
                Actions hover = new Actions(driver);

                hover.moveToElement(myAccount).build().perform();

                //Clicar el botó My Account
                myAccount.click();

                //Mouse over LoginMobile
                hover.moveToElement(loginMenu).build().perform();

                //Clicar menu LoginMobile
//                wait.until(ExpectedConditions.elementToBeClickable(loginMenu));
                loginMenu.click();

                //CLicar camp text email de Returning Customer
                loginEmail.click();

                //Escriure usuari valid
                loginEmail.sendKeys(compte);

                //Clicar el camp de text password
                loginPassword.click();

                //Escriure password valid
                loginPassword.sendKeys(password);

                //Mouse over botó de LoginMobile
                hover.moveToElement(loginButton).build().perform();

                //Clic a LoginMobile
                loginButton.click();



        }
}

