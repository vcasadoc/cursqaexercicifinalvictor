package com.itnove.ba.opencart.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CheckoutPage {


        private WebDriver driver;
        public WebDriverWait wait;


        public String valid_email = "vic.casado@gmail.com";
        public String invalid_email = "test@test.com";
        public String valid_password = "validpassword";
        public String invalid_password = "invalidpassword";



        @FindBy(xpath="//*[@id='input-payment-password']")
        public WebElement textPassword;

        @FindBy(xpath="//*[@id='input-payment-confirm']")
        public WebElement textConfirmPassword;

        @FindBy(xpath="//*[@id='button-register']")
        public WebElement buttonRegister;

        @FindBy(xpath="//*[@id='collapse-payment-address']/div/div[3]/div/input[1]")
        public WebElement checkAgreement;

        @FindBy(xpath="//*[@id='collapse-checkout-option']/div/div/div[1]/div[1]/label/input")
        public WebElement radioRegisterAccount;

        @FindBy(xpath="//*[@id='top-links']/ul/li[5]/a/span")
        public WebElement checkoutButton;

        @FindBy(xpath = "//*[@id='content']/p")
        public WebElement missatgeCarroBuit;

        @FindBy(xpath="//*[@id='content']/div[2]/div[1]/div/div[3]/button[1]")
        public WebElement addToCartMac;

        @FindBy(xpath="//*[@id='common-home']/div[1]")
        public WebElement successMessage;
        //Success: You have added MacBook to your shopping cart!

        @FindBy(xpath="//*[@id='collapse-checkout-option']/div/div/div[1]/div[2]/label/input")
        public WebElement radioGuest;

        @FindBy(xpath="//*[@id='input-email']")
        public WebElement customerEmail;

        @FindBy(xpath="//*[@id='input-password']")
        public WebElement customerPassword;

        @FindBy(xpath="//*[@id='button-login']")
        public WebElement customerLoginButton;

        @FindBy(xpath="//*[@id='collapse-checkout-option']/div/div[1]")
        public WebElement errorCustomerLogin;
        // Warning: No match for E-Mail Address and/or Password.

        @FindBy(xpath="//*[@id='button-account']")
        public WebElement continueGuest;

        @FindBy(xpath="//*[@id='input-payment-firstname']")
        public WebElement textName;

        @FindBy(xpath="//*[@id='input-payment-lastname']")
        public WebElement textLastName;

        @FindBy(xpath="//*[@id='input-payment-email']")
        public WebElement textEmail;

        @FindBy(xpath="//*[@id='input-payment-telephone']")
        public WebElement textPhone;

        @FindBy(xpath="//*[@id='input-payment-address-1']")
        public WebElement textAddress1;

        @FindBy(xpath="//*[@id='input-payment-city']")
        public WebElement textCity;

        @FindBy(xpath="//*[@id='input-payment-postcode']")
        public WebElement textPostCode;

        @FindBy(xpath="//*[@id='input-payment-country']")
        public WebElement countryDisplay;

        @FindBy(xpath="//*[@id='input-payment-country']/option[209]")
        public WebElement countrySpain;

        @FindBy(xpath="//*[@id='input-payment-zone']")
        public WebElement regionDisplay;

        @FindBy(xpath="//*[@id='input-payment-zone']/option[10]")
        public WebElement regionBarcelona;

        @FindBy(xpath="//*[@id='button-guest']")
        public WebElement continueToPaymentMethod;

        @FindBy(xpath="//*[@id='collapse-payment-method']/div/div[2]/div/input[1]")
        public WebElement checkTerms;

        @FindBy(xpath="//*[@id='button-payment-method']")
        public WebElement continueToConfirmOrder;

        @FindBy(xpath="//*[@id='collapse-payment-method']/div/div[1]")
        public WebElement errorPaymentMethod;
        //Warning: Payment method required!

        @FindBy(xpath="//*[@id='account']/div[2]/div")
        public WebElement errorFirstName;

        @FindBy(xpath="//*[@id='account']/div[3]/div")
        public WebElement errorLastName;

        @FindBy(xpath="//*[@id='account']/div[4]/div")
        public WebElement errorEmail;

        @FindBy(xpath="//*[@id='account']/div[5]/div")
        public WebElement errorPhone;

        @FindBy(xpath="//*[@id='address']/div[2]/div")
        public WebElement errorAddress1;

        @FindBy(xpath="//*[@id='address']/div[4]/div")
        public WebElement errorCity;

        @FindBy(xpath="//*[@id='address']/div[5]/div")
        public WebElement errorPostCode;

        @FindBy(xpath="//*[@id='address']/div[7]/div")
        public WebElement errorRegion;

        @FindBy(xpath="//*[@id='payment-existing']/select")
        public WebElement addressDisplay;




        public CheckoutPage(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }

        public void omplirCamps(String nom, String cognom, String email, String telefon, String address, String ciutat, String CodiPostal, WebDriver driver) {


                Actions hover = new Actions(driver);
                //Clicar al camp de text First Name


                textName.click();

                //Escriure John
                textName.sendKeys(nom);

                //Clicar al camp de text Last Name
                textLastName.click();

                //Escriure Doe
                textLastName.sendKeys(cognom);

                //Clicar al camp de text email
                textEmail.click();

                //Escriure prova@prova.com
                textEmail.sendKeys(email);

                //Clicar al camp de text Telephone
                textPhone.click();

                //Escriure 777777777
                textPhone.sendKeys(telefon);

                //Clicar al camp de text Address1
                textAddress1.click();

                //Escriure Test address 1
                textAddress1.sendKeys(address);

                //Clicar al camp de text City
                textCity.click();

                //Escriure Barcelona
                textCity.sendKeys(ciutat);

                //Clicar al camp de text Post Code
                textPostCode.click();

                //Escriure 08080
                textPostCode.sendKeys(CodiPostal);

                //Fer clic al desplegable Country
                countryDisplay.click();

                //CLicar Spain
                countrySpain.click();

                //Fer clic al desplegable Region / State
                regionDisplay.click();

                //CLicar Barcelona
                regionBarcelona.click();

                //Situar Mouse sobre botó Continue
                hover.moveToElement(continueToPaymentMethod).build().perform();

                //Clicar Continue
                continueToPaymentMethod.click();

                //Clicar el checkbox de la seccio I Have read...
                wait.until(ExpectedConditions.elementToBeClickable(checkTerms));

                checkTerms.click();

                //Situar mouse sobre Continue
                hover.moveToElement(continueToConfirmOrder).build().perform();

                //Clicar continue
                continueToConfirmOrder.click();


        }


}

