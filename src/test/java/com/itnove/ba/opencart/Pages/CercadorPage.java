package com.itnove.ba.opencart.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CercadorPage {


        private WebDriver driver;

        @FindBy(xpath="id('cercador')/input[1]")
        public WebElement cercador;

        @FindBy(xpath = "//*[@id='cercador']/span/button")
        public WebElement lupa;

        @FindBy(xpath = "//*[@id='content']/div[3]/div[1]/div/div[2]/div[1]/h4/a")
        public WebElement resultat;

        @FindBy(xpath = "//*[@id='content']/p[2]")
        public WebElement missatgeCarroBuit;

        public CercadorPage(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }


}

